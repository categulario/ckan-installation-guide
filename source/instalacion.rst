Instalación
===========

Esta es una instalación desde el código fuente, se puede consultar la
`documentación sobre métodos de instalación`_ para más opciones. Algunas partes
de la instalación las he desviado a propósito del método propuesto para
simplificar el proceso de instalación y/o el mantenimiento del sistema en
producción.

Consideraciones técnicas y preparación
--------------------------------------

Estas van a ser las decisiones previas para llevar a buen término de este
proyecto:

* Sistema operativo Ubuntu 20.04 (última LTS al momento de escritura de este
  documento)
* 4 Gb RAM, 2 CPU, 80 GB SSD ($30 dólares en linode)

Crear un usuario no-root, que pertenezca al grupo ``sudo`` (o pueda ejecutar
comandos con sudo). Este usuario va a realizar la instalación. Este usuario
debería pertenecer además al grupo ``adm``.

La instalación se realizará dentro de ``/opt`` en vez de ``/usr/lib``.

Usaremos extensivamente las capacidades de `Systemd`_ para configurar los
diferentes servicios y temporizadores que requiere la plataforma.

Discos externos
...............

Para evitar que el alto volumen de archivos en ``/`` interfiera con la
instalación es importante contar con un volumen o partición externa montada en
el servidor. En el caso de un servidor físico esto se puede lograr creando una
partición dedicada al almacenamiento de datos y montada en alguna parte. En el
caso de usar un proveedor de nube es importante que este volumen esté igualmente
montado en alguna ruta conocida del servidor.

Dicha configuración varía mucho entre proveedor y proveedor y está fuera de los
límites de este manual. En adelante se considerará que dicha partición está
montada bajo la ruta ``/mnt/ckan-data``.

Nombres de dominio
..................

Vale la pena tener un dominio ya apuntado a esta instalación para comenzar.

Extensiones
-----------

* Filestore
* Datastore
* Datapusher

De visualización (http://docs.ckan.org/en/2.9/maintaining/data-viewer.html)

* recline_view
* recline_grid_view
* recline_graph_view
* recline_map_view
* text_view
* image_view
* webpage_view

Check these out http://docs.ckan.org/en/2.9/maintaining/data-viewer.html#other-view-plugins

Proceso
-------

Se instalan los paquetes necesarios

.. code:: shell-session

   $ sudo apt-get install -y python3-dev postgresql libpq-dev python3-pip git-core redis-server solr-jetty virtualenv nginx

Creamos al usuario que va a manejar la instalación (distinto del usuario que
usamos para instalar, por seguridad).

.. code:: shell-session

   $ sudo mkdir -p /opt/ckan
   $ sudo mkdir -p /etc/ckan
   $ sudo useradd --home-dir /opt/ckan --system --shell /bin/bash --user-group ckan
   $ sudo chown ckan:ckan /opt/ckan /etc/ckan

Cambiamos al nuevo usuario para seguir con la instalación

.. code:: shell-session

   $ sudo su ckan
   [ckan@server]$ cd /opt/ckan
   [ckan@server]$ git clone https://github.com/ckan/ckan.git ckan-code
   [ckan@server]$ cd ckan-code
   [ckan@server]$ git checkout ckan-2.9.5
   [ckan@server]$ virtualenv ../ckan-venv
   [ckan@server]$ source ../ckan-venv/bin/activate
   (ckan-venv)[ckan@server]$ pip install -e .
   (ckan-venv)[ckan@server]$ pip install -r requirements.txt
   (ckan-venv)[ckan@server]$ deactivate

Creamos un usuario de postgresql desde nuestra cuenta de administración del
servidor, anotamos la contraseña que usemos.

.. code:: shell-session

   $ sudo -u postgres createuser -P ckan

Y una base de datos

.. code:: shell-session

   $ sudo -u postgres createdb -O ckan ckan -E utf-8

Configuración de Solr
---------------------

.. code:: shell-session

   $ sudo mv /etc/solr/conf/schema.xml{,.bak}
   $ sudo ln -s /opt/ckan/ckan-code/ckan/config/solr/schema.xml /etc/solr/conf/schema.xml
   $ sudo systemctl restart jetty9

Configuración de CKAN
---------------------

En este momento es necesario asegurar que un volumen/partición existe como se
mencionó en `Discos externos`_. Lo que procede es asegurar que el usuario
``ckan`` puede escribir en este volumen ajustando los permisos:

.. code:: shell-session

   $ sudo chown ckan:ckan /mnt/ckan-data

Ahora generaremos el archivo de configuración de CKAN y añadiremos las
credenciales de postgresql previamente generadas.

.. code:: shell-session

   [ckan@server]$ source /opt/ckan/ckan-venv/bin/activate
   (ckan-venv)[ckan@server]$ ckan generate config /etc/ckan/ckan.ini

Copiaremos el archivo de configuración ``who.ini`` al directorio de instalación:

.. code:: shell-session

   [ckan@server]$ cp /opt/ckan/ckan-code/who.ini /etc/ckan/

Y editaremos ``/etc/ckan/ckan.ini`` con nuestro editor favorito, ubicando las siguientes
llaves y escribiendo los valores adecuados. Por lo menos las siguientes llaves
es necesario editar:

.. code:: ini

   sqlalchemy.url = postgresql://ckan:pass@localhost/ckan
   ckan.storage_path = /mnt/ckan-data
   ckan.site_id = my_ckan_site
   ckan.site_url = http://12.34.56.78
   solr_url = http://127.0.0.1:8080/solr
   who.config_file = /etc/ckan/who.ini

Con ese archivo guardado procedemos a crear la base de datos:

.. code:: shell-session

   [ckan@server]$ source /opt/ckan/ckan-venv/bin/activate
   [ckan@server]$ ckan -c /etc/ckan/ckan.ini db init

Configuración del Datastore
---------------------------

Ahora procederemos a configurar algunas extensiones indispensables para ckan,
como es el datastore.

Lo primero será habilitar el plugin en la lista de plugins dentro de
``/etc/ckan/ckan.ini`` buscando por la llave ``ckan.plugins`` y añadiendo, sin
quitar elementos, ``datastore`` a la lista. Quedaría algo así:

.. code:: ini

   ckan.plugins = stats text_view image_view recline_view datastore

Este plugin necesita una base de datos adicional a la base de datos de la
instalación, así como un usuario nuevo.

.. code:: shell-session

   $ sudo -u postgres createuser -S -D -R -P -l ckan_datastore
   $ sudo -u postgres createdb -O ckan ckan_datastore -E utf-8

Nótese que la nueva base de datos es propiedad del usuario ``ckan`` de postgres
que creamos primero. Los comandos de arriba nos pidieron una contraseña para el
usuario ``ckan_datastore``. Usaremos esta información para editar en
``ckan.ini`` en las llaves ``ckan.datastore.write_url`` y
``ckan.datastore.read_url`` como se muestra a continuación.

.. code:: ini

   ckan.datastore.write_url = postgresql://ckan:pass@localhost/ckan_datastore
   ckan.datastore.read_url = postgresql://ckan_datastore:pass@localhost/ckan_datastore

Naturalmente reemplazando ``pass`` por la contraseña elegida para el usuario
``ckan`` y ``ckan_datastore`` respectivamente.

A continuación estableceremos los permisos necesarios para una operación segura
de la base de datos de datastore:

.. code:: shell-session

   [ckan@server]$ source /opt/ckan/ckan-venv/bin/activate
   (ckan-venv)[ckan@server]$ ckan -c /etc/ckan/ckan.ini datastore set-permissions > /tmp/perms.sql
   $ sudo -u postgres psql --set ON_ERROR_STOP=1 < /tmp/perms.sql
   $ sudo rm /tmp/perms.sql

Habilitar el servicio web
-------------------------

Hasta ahora no hemos podido ver realmente nada en funcionamiento. Llegó la hora.

Lo primero será copiar el script wsgi que viene con ckan:

.. code:: shell-session

   [ckan@server]$ cp /opt/ckan/ckan-code/wsgi.py /opt/ckan/

Activamos el entorno virtual e instalamos ``uwsgi``, que fungirá como servidor
de producción de CKAN:

.. code:: shell-session

   [ckan@server]$ source /opt/ckan/ckan-venv/bin/activate
   (ckan-venv)[ckan@server]$ pip install uwsgi
   [ckan@server]$ touch /etc/ckan/uwsgi.ini

Y ahora editaremos el archivo ``/etc/ckan/uwsgi.ini`` con los siguientes
contenidos:

.. code:: ini

   [uwsgi]

   socket = 127.0.0.1:8081
   uid = ckan
   gid = ckan
   wsgi-file = /opt/ckan/wsgi.py
   virtualenv = /opt/ckan/ckan-venv
   env = CKAN_INI=/etc/ckan/ckan.ini
   module = wsgi:application
   master = true
   pidfile = /run/ckan/ckan.pid
   harakiri = 50
   max-requests = 5000
   vacuum = true
   callable = application
   buffer-size = 32768
   strict = true
   req-logger = file:/var/log/ckan/access.log
   workers = 4
   threads = 2
   lazy-apps = true

Y finalmente el archivo de Systemd que gestionará este servicio. Lo ubicaremos
en ``/etc/systemd/system/ckan.service`` con los siguientes contenidos:

.. code:: ini

   [Unit]
   Description=Manager for CKAN's uwsgi http service
   After=network.target

   [Service]
   PermissionsStartOnly=true
   ExecStartPre=/bin/mkdir -p /run/ckan/
   ExecStartPre=/bin/mkdir -p /var/log/ckan/
   ExecStartPre=/bin/chown -R ckan:ckan /run/ckan/
   ExecStartPre=/bin/chown -R ckan:ckan /var/log/ckan/
   PIDFile=/run/ckan/ckan.pid
   User=ckan
   Group=ckan
   WorkingDirectory=/opt/ckan
   ExecStart=/opt/ckan/ckan-venv/bin/uwsgi -i /etc/ckan/uwsgi.ini
   ExecReload=/bin/kill -s HUP $MAINPID
   Restart=on-failure
   KillSignal=SIGQUIT
   Type=notify
   NotifyAccess=all

   [Install]
   WantedBy=multi-user.target

Recargamos los servicios e iniciamos el servicio web de ckan:

.. code:: shell-session

   $ sudo systemctl daemon-reload
   $ sudo systemctl enable --now ckan

Para concluir esta sección editaremos la configuración de Nginx para
redireccionar las peticiones a nuestro servicio uWsgi de CKAN:

.. code:: shell-session

   $ sudo mv /etc/nginx/sites-available/{default,ckan}
   $ sudo rm /etc/nginx/sites-enabled/default
   $ sudo ln -s /etc/nginx/sites-{available,enabled}/ckan

Editaremos el archivo ``/etc/nginx/sites-available/ckan`` con los siguientes
contenidos (reemplazando los anteriores):

.. code:: nginx

   uwsgi_cache_path /tmp/nginx_cache levels=1:2 keys_zone=cache:30m max_size=250m;
   uwsgi_temp_path /tmp/nginx_proxy 1 2;

   server {
       listen 80 default_server;
       listen [::]:80 default_server;

       client_max_body_size 100M;

       location / {
           uwsgi_pass 127.0.0.1:8081;

           include uwsgi_params;

           uwsgi_cache cache;
           uwsgi_cache_bypass $cookie_auth_tkt;
           uwsgi_no_cache $cookie_auth_tkt;
           uwsgi_cache_valid 30m;
           uwsgi_cache_key $host$scheme$proxy_host$request_uri;
           # In emergency comment out line to force caching
           # uwsgi_ignore_headers X-Accel-Expires Expires Cache-Control;
       }
   }

Recargamos nginx

.. code:: shell-session

   $ sudo systemctl reload nginx

Y ahora podemos visitar la dirección IP del servidor desde el navegador y ver
nuestra instancia de CKAN funcionando.

Creación de un usuario sysadmin
-------------------------------

.. highlights::

   ¡Atención! El proceso de instalación crea un usuario sysadmin con el nombre
   usado en ``ckan.site_id``. Es importante no borrar este usuario pues es el
   que usa Datapusher para hacer las peticiones a la API de CKAN.

Para manejar la instalación es importante contar con uno (o varios) usuarios que
puedan realizar tareas administrativas. El proceso es muy sencillo:

.. code:: shell-session

   [ckan@server]$ source /opt/ckan/ckan-venv/bin/activate
   (ckan-venv)[ckan@server]$ ckan -c /etc/ckan/ckan.ini sysadmin add <username> email=<email> name=<username>

Nótese el uso repetido del username. Ese comando pedirá por una contraseña que
podrás usar para acceder al portal web de tu instalación de ckan y realizar
acciones administrativas.

También es posible tomar un usuario existente y convertirlo en sysadmin de la
siguiente manera:

.. code:: shell-session

   [ckan@server]$ source /opt/ckan/ckan-venv/bin/activate
   (ckan-venv)[ckan@server]$ ckan -c /etc/ckan/ckan.ini sysadmin add <username>

Configuración del envío de correos
----------------------------------

CKAN puede enviar correos con mensajes de error y otro tipo de correos que son
parte de su operación (por ejemplo para la recuperación de una cuenta de
correo). Para configurar el envío de correos es necesario contar con un
proveedor de envío de correos que acepte el protocolo SMTP. Este proveedor puede
ser un proveedor de nube, o para algunos usos muy simples, una cuenta de correo
ordinaria de gmail.

Para esta configuración se asumirá el último caso, pero opciones similares se
pueden usar para configurar envíos con Amazon SES, Sendgrid, Mailgun o algún
otro proveedor, incluso un servicio propio de envío de correos cuya
configuración está fuera de los límites de este manual. Para mayor información
consultar la `documentación de ckan respecto a envío de correos`_.

Editaremos el archivo ``/etc/ckan/ckan.ini`` y modificaremos las siguientes
llaves con nuestras credenciales:

.. code:: ini

   # Esto debe ser una cuenta de correo existente que esté en nuestro control y
   # a la que solamente personal de confianza tenga acceso, pues recibirá los
   # reportes de error de CKAN que podrían contener información sensible
   email_to = errors-ckan@gmail.com

   # Esta es la dirección de correo que aparecerá en el campo FROM de los
   # correos enviados
   error_email_from = ckan-errors@gmail.com

   # Y estas son las configuraciones del servidor SMTP. A continuación algunos
   # valores básicos para Gmail.
   smtp.server = smtp.gmail.com
   smtp.starttls = True
   smtp.user = existent-user@gmail.com
   smtp.password = 3ey278syner2378
   smtp.mail_from = existent-user@gmail.com
   smtp.reply_to = existent-user@gmail.com

Finalmente reiniciamos el servicio de CKAN

.. code:: shell-session

   $ sudo systemctl reload ckan

Notificaciones de actividad
...........................

Hay correos (por ejemplo los relativos a la actividad de la plataforma) que para
ser enviados se necesita un servicio. Ahora configuraremos ese servicio. Como
muchas cosas de esta guía usaremos la característica de `timers de systemd`_
para esto.

Para enviar los correos es necesario hacer una petición a la `API HTTP de CKAN
para envío de notificaciones`_, lo cual requiere de un token que podemos obtener
por medio de la interfaz web.

1. Iniciar sesión en la plataforma web.
2. Dar click en nuestro nombre de usuario en la barra superior.
3. Seleccionar la pestaña ``Api tokens``.
4. Escoger un nombre para el token.
5. Presionar el botón crear.

Como resultado de esto se nos mostrará en un mensaje el token que se creó, el
cual debemos copiar y despúes utilizar para la creación del archivo
``/etc/ckan/email-service.env`` con contenidos similares a los siguientes:

.. code:: bash

   CKAN_AUTH_HEADER=Authorization: eltokenquecopiamosdelsitio

Reemplazando ``eltokenquecopiamosdelsitio`` por el token que copiamos del sitio.

Necesitaremos también editar ``/etc/ckan/ckan.ini`` y habilitar los correos de
notificación:

.. code:: ini

   ckan.activity_streams_email_notifications = true

Reiniciamos ckan:

.. code:: shell-session

   $ sudo systemctl reload ckan

Luego crearemos el archivo de servicio en ``/etc/systemd/system/ckan-emails.service``
con los siguientes contenidos:

.. code:: ini

   [Unit]
   Description=Sends CKANs email notifications

   [Service]
   Type=oneshot
   EnvironmentFile=/etc/ckan/email-service.env
   ExecStart=/usr/bin/curl -H ${CKAN_AUTH_HEADER} -X POST -s -d "{}" http://localhost/api/action/send_email_notifications
   User=ckan
   Group=ckan

   [Install]
   WantedBy=multi-user.target

Y luego el timer correspondiente en ``/etc/systemd/system/ckan-emails.timer``:

.. code:: ini

   [Unit]
   Description=Timer for CKANs email notifications

   [Timer]
   OnCalendar=hourly
   Persistent=true

   [Install]
   WantedBy=timers.target

Finalmente activaremos el timer:

.. code:: shell-session

   $ sudo systemctl enable --now ckan-emails.timer

Tracking de visitas
-------------------

TODO http://docs.ckan.org/en/2.9/maintaining/tracking.html

Instalación y configuración de Datapusher
-----------------------------------------

Datapusher es una extensión de CKAN que utiliza su propio servicio web para
importar ciertos tipos de archivo (como tabulares) en tablas de una base de
datos. Esto permite visualizaciones que de otra forma serían más complicadas de
lograr.

Esta sección de la guía de instalación se basa en la `documentación de
datapusher`_.

Comenzaremos creando una base de datos propia de datapusher. Usamos una
contraseña segura y la anotamos.

.. code:: shell-session

   $ sudo -u postgres createuser -S -D -R -P datapusher_jobs
   $ sudo -u postgres createdb -O datapusher_jobs datapusher_jobs -E utf-8

Luego crearemos un entorno virtual para datapusher, independiente de aquel de
ckan, e instalamos algunas dependencias necesarias.

.. Aunque la documentación a la fecha recomienda el uso de una versión
   etiquetada la realidad es que la última versión etiquetada no permite el uso
   de variables de entorno para la configuración, por lo tanto usaré un commit
   específico en el que sí es posible.

   https://github.com/ckan/datapusher/issues/245

.. code:: shell-session

   [ckan@server]$ virtualenv /opt/ckan/datapusher-venv
   [ckan@server]$ git clone https://github.com/ckan/datapusher.git /opt/ckan/datapusher-code
   [ckan@server]$ cd /opt/ckan/datapusher-code
   [ckan@server]$ git checkout 8d95fea
   [ckan@server]$ source /opt/ckan/datapusher-venv/bin/activate
   (datapusher-venv)[ckan@server]$ pip install --upgrade pip
   (datapusher-venv)[ckan@server]$ pip install -r /opt/ckan/datapusher-code/requirements.txt
   (datapusher-venv)[ckan@server]$ pip install -e /opt/ckan/datapusher-code/
   (datapusher-venv)[ckan@server]$ pip install uwsgi psycopg2-binary
   (datapusher-venv)[ckan@server]$ touch /etc/ckan/datapusher-uwsgi.ini
   (datapusher-venv)[ckan@server]$ deactivate

Editaremos ahora el archivo ``/etc/ckan/datapusher-uwsgi.ini`` añadiendo los
siguientes contenidos, cuidando que las credenciales de acceso a la base de
datos de datapusher coincidan con las que creamos en los pasos anteriores.

.. code:: ini

   [uwsgi]

   http = 127.0.0.1:8800
   uid = ckan
   guid = ckan
   wsgi-file = /opt/ckan/datapusher-code/deployment/datapusher.wsgi
   virtualenv = /opt/ckan/datapusher-venv
   master = true
   pidfile = /run/ckan/datapusher.pid
   harakiri = 50
   max-requests = 5000
   vacuum = true
   callable = application
   buffer-size = 32768
   env = DATAPUSHER_SQLALCHEMY_DATABASE_URI=postgresql://datapusher_jobs:PASS@localhost/datapusher_jobs
   workers = 4
   threads = 2
   lazy-apps = true

Ahora creamos un archivo ``.service`` para manejar este servicio:

.. code:: shell-session

   $ sudo touch /etc/systemd/system/datapusher.service

Y lo editamos con los siguientes contenidos:

.. code:: ini

   [Unit]
   Description=Manager for the datapusher service
   After=network.target

   [Service]
   PermissionsStartOnly=true
   ExecStartPre=/bin/mkdir -p /run/ckan/
   ExecStartPre=/bin/mkdir -p /var/log/ckan/
   ExecStartPre=/bin/chown -R ckan:ckan /run/ckan/
   ExecStartPre=/bin/chown -R ckan:ckan /var/log/ckan/
   PIDFile=/run/ckan/datapusher.pid
   User=ckan
   Group=ckan
   WorkingDirectory=/opt/ckan
   ExecStart=/opt/ckan/datapusher-venv/bin/uwsgi -i /etc/ckan/datapusher-uwsgi.ini
   ExecReload=/bin/kill -s HUP $MAINPID
   Restart=on-failure
   KillSignal=SIGQUIT
   Type=notify
   NotifyAccess=all

   [Install]
   WantedBy=multi-user.target

Recargamos los demonios en systemd e iniciamos el servicio de datapusher,
indicando que se inicie al iniciar el sistema.

.. code:: shell-session

   $ sudo systemctl daemon-reload
   $ sudo systemctl enable --now datapusher

Con el servicio de datapusher corriendo ahora podemos ir a la configuración de
CKAN y activar el plugin. Editamos ``/etc/ckan/ckan.ini`` y añadimos
``datapusher`` a la llave ``ckan.plugins`` sin quitar aquellos que ya estén ahí.
También es necesario ajustar la dirección en la que se encuentra datapusher.
En mi caso se ve así:

.. code:: ini

   ckan.plugins = stats text_view image_view recline_view datastore datapusher
   ckan.datapusher.url = http://127.0.0.1:8800

Hecho esto reiniciamos el servicio de ckan

.. code:: shell-session

   $ sudo systemctl reload ckan

Tareas en segundo plano
-----------------------

CKAN provee un mecanismo para ejecutar tareas que serían muy pesadas de realizar
por el servicio web. A continuación lo configuraremos.

En realidad lo único que necesitamos es un archivo ``.service`` ubicado en
``/etc/systemd/system/ckan-background@.service`` (nótese la arroba en el nombre
del archivo) con los siguientes contenidos:

.. code:: ini

   [Unit]
   Description=Manager for ckan background tasks worker (%i)
   After=network.target

   [Service]
   User=ckan
   Group=ckan
   WorkingDirectory=/opt/ckan
   ExecStart=/opt/ckan/ckan-venv/bin/ckan -c /etc/ckan/ckan.ini jobs worker
   Restart=on-failure
   Type=simple

   [Install]
   WantedBy=multi-user.target

Iniciaremos este servicio, indicando que arranque al iniciar el sistema de la
siguiente manera:

.. code:: shell-session

   $ sudo systemctl enable --now ckan-background@worker1

Este servicio tiene la peculiaridad de permitir levantar una `instancia del
servicio`_ o varias. En este caso solamente levantamos una, identificada con el
nomre ``worker1``, sin embargo podríamos, de ser necesario, levantar más usando
otros nombres, por instancia:

.. code:: shell-session

   $ sudo systemctl enable --now ckan-background@worker2

Configuración de los plugins de visualización
---------------------------------------------

Podría ser relevante activar una vista de JSON para archivos GEOJSON (aunque no
es necesario):

.. code:: ini

   ckan.preview.json_formats = json geojson

Visualización de PDF
....................

Desde la documentación de `ckanext-pdfview`_.

.. code:: shell-session

   [ckan@server]$ source /opt/ckan/ckan-venv/bin/activate
   (ckan-venv)[ckan@server]$ pip install ckanext-pdfview

Editamos ``/etc/ckan/ckan.ini`` y añadimos ``pdf_view`` a la lista de plugins,
así como a la lista de visualizaciones que se generan por defecto:

.. code:: ini

   ckan.plugins = .. pdf_view
   ckan.views.default_views = .. pdf_view

Finalmente reiniciamos el servicio ``ckan``.

.. code:: shell-session

   $ sudo systemctl reload ckan

Visualización de archivos GeoJSON
.................................

Desde la documentación de `ckanext-geoview`_.

.. code:: shell-session

   [ckan@server]$ mkdir -p /opt/ckan/plugins
   [ckan@server]$ git clone https://github.com/ckan/ckanext-geoview.git /opt/ckan/plugins/ckanext-geoview
   [ckan@server]$ cd /opt/ckan/plugins/ckanext-geoview
   [ckan@server]$ git checkout a1a1720
   [ckan@server]$ source /opt/ckan/ckan-venv/bin/activate
   (ckan-venv)[ckan@server]$ pip install -e .
   (ckan-venv)[ckan@server]$ pip install -r pip-requirements.txt
   (ckan-venv)[ckan@server]$ deactivate

Y recargamos el servicio de ckan

.. code:: shell-session

   $ sudo systemctl reload ckan

Almacenamiento en S3
--------------------

PENDIENTE https://github.com/TkTech/ckanext-cloudstorage

.. _documentación sobre métodos de instalación: http://docs.ckan.org/en/2.9/maintaining/installing/index.html
.. _documentación de instalación de podman: https://podman.io/getting-started/installation
.. _documentación de archlinux sobre podman: https://wiki.archlinux.org/title/Podman#Rootless_Podman
.. _documentación de arch sobre systemd: https://wiki.archlinux.org/title/Systemd/User#Automatic_start-up_of_systemd_user_instances
.. _documentación de datapusher: https://github.com/ckan/datapusher#production-deployment
.. _instancia del servicio: http://0pointer.net/blog/projects/instances.html
.. _timers de systemd: https://wiki.archlinux.org/title/Systemd/Timers
.. _documentación de ckan respecto a envío de correos: http://docs.ckan.org/en/2.9/maintaining/email-notifications.html
.. _Systemd: https://wiki.archlinux.org/title/Systemd
.. _API HTTP de ckan para envío de notificaciones: http://docs.ckan.org/en/2.9/api/index.html#ckan.logic.action.update.send_email_notifications
.. _ckanext-pdfview: https://github.com/ckan/ckanext-pdfview
.. _ckanext-geoview: https://github.com/ckan/ckanext-geoview
