Mantenimiento y depuración
==========================

Servicios
---------

Todos ellos operables bajo ``systemd``. (``systemctl status <servicio>``)

* ``jetty9`` Responsable de apache solr
* ``ckan`` Servicio web de ckan
* ``datapusher`` Datapusher
* ``ckan-background`` Tareas en segundo plano

Mencionar ``journalctl -f --unit <servicio>``. Estar en el grupo ``adm``.

Archivos de registro
--------------------

* jetty
* ckan-uwsgi
