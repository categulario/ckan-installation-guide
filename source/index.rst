.. Guía de instalación de CKAN documentation master file, created by
   sphinx-quickstart on Sat Feb 26 12:10:58 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Guía de instalación de CKAN
===========================

bla bla bla

.. toctree::
   :maxdepth: 2
   :caption: Contenidos:

   instalacion.rst
   adaptacion_de_la_interfaz.rst
   perfil_de_seguridad.rst
   mantenimiento_y_depuracion.rst
   uso_de_la_plataforma.rst

Tablas e índices
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
