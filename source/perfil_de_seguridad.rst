Perfil de seguridad
===================

Para mantener segura esta instalación es importante tomar en cuenta los puertos
y protocols que quedan expuestos al mundo.

Puertos
-------

Estos son los puertos que usa la instalación actual y que quedan expuestos al
mundo:

* ``8080`` Apache Solr. No es necesario que esté expuesto al internet.
* ``80`` Nginx. Necesario.
* ``22`` ssh. Necesario.

Es recomendable configurar un firewall que permita solamente entrada en los
puertos ``22``, ``80`` y ``443`` (en caso de configurar SSL).

Puertos internos
................

Están en uso algunos puertos que no están por defecto expuestos al mundo, sin
embargo es importante estar al tanto de ellos.

* ``8081`` uWsgi
* ``5432`` Postgresql
* ``6379`` Redis

Usuarios
--------

La mayor parte de la gestión de la instalación es realizada por el usuario no
privilegiado ``ckan``. Solamente algunos servicios bien conocidos como Nginx son
operados por otros usuarios.
